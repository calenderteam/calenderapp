package com.beeckmann.sven.planit;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Sven on 10.05.2015.
 */
public class CalenderGridAdapter extends BaseAdapter {
    private List<String> days;
    private GregorianCalendar calender;
    private Context context;

    public CalenderGridAdapter(Context context, GregorianCalendar calender)
    {
        this.context = context;
        this.calender = calender;
        days = new ArrayList<String>();
    }

    @Override
    public int getCount() {
        return days.size();
    }

    @Override
    public Object getItem(int position) {
        return days.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView = convertView;
        TextView dayView;
        if(convertView == null)
        {
            gridView = new View(context);
            gridView = inflater.inflate(R.layout.fragment_calendar_cell, parent, false);
        }

        dayView = (TextView) gridView.findViewById(R.id.date);
        dayView.setText(days.get(position));
        return gridView;
    }

    public void refreshDays()
    {
        days.clear();
        int numberOfDays = calender.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
        int currentDay = calender.get(GregorianCalendar.DAY_OF_MONTH);
        GregorianCalendar tempCalender = (GregorianCalendar)calender.clone();
        buildPreviousMonthDays(tempCalender);
        buildCurrentMonthDays(calender);

    }

    /**
     * builds the days needed to the view bevor the first of this month because we always want the view to
     * start on mondays and if the first of the currently active month is not monday we need to add days from the month
     * bevor
     * @param calender
     */
    private void buildPreviousMonthDays(GregorianCalendar calender)
    {
        int firstDay = getFirstDay(calender);
        calender.add(GregorianCalendar.DAY_OF_MONTH, -1 * firstDay);
        for(int i = 0 ; i < firstDay ; i++)
        {
            int day = calender.get(Calendar.DAY_OF_MONTH);
            days.add(Integer.toString(day));
            calender.add(GregorianCalendar.DAY_OF_MONTH, 1);
        }
    }

    private void buildCurrentMonthDays(GregorianCalendar calender)
    {
        for(int i = 1; i < calender.getActualMaximum(GregorianCalendar.DAY_OF_MONTH) + 1; i++)
        {
            if( i < 10)
            {
                days.add("0" + (i));
            }
            else
            {
                days.add(Integer.toString(i));
            }
        }
    }

    private int getFirstDay(GregorianCalendar calender)
    {
        // set this calender to the current month and the first day of the month
        calender.set(GregorianCalendar.DAY_OF_MONTH,1);

        // normal counting starts with Sunday with a value of 1
        // redefined to start the week at Monday with value of 0
        switch (calender.get(GregorianCalendar.DAY_OF_WEEK))
        {
            case GregorianCalendar.MONDAY:
                return 0;
            case GregorianCalendar.TUESDAY:
                return 1;
            case GregorianCalendar.WEDNESDAY:
                return 2;
            case GregorianCalendar.THURSDAY:
                return 3;
            case GregorianCalendar.FRIDAY:
                return 4;
            case GregorianCalendar.SATURDAY:
                return 5;
            case GregorianCalendar.SUNDAY:
                return 6;
        }
        // error
        return -1;
    }
}
