package com.beeckmann.sven.planit;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.astuetz.PagerSlidingTabStrip;

/**
 * Created by Sven on 14.05.2015.
 */
public class SampleFragmentPagerAdapter extends FragmentPagerAdapter implements PagerSlidingTabStrip.IconTabProvider {
    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[] { "Tab1", "Tab2", "Tab3" };
    // TODO hier müssen andere Ressourcen genommen werden da dies nicht best practice ist interne Ressourcen zu benuzten da diese sich ändern können
    // brauchen noch mehr icons also friends and notifications vermutlich
    private int tabIcons[] = {android.R.drawable.ic_menu_my_calendar, android.R.drawable.ic_menu_my_calendar, android.R.drawable.ic_menu_my_calendar};
    public SampleFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return new MainActivity.PlaceholderFragment();
    }
/*
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }*/

    @Override
    public int getPageIconResId(int i) {
        return tabIcons[i];
    }
}

