package com.beeckmann.sven.planit;

import android.support.v4.view.ViewPager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;

import java.util.GregorianCalendar;


public class MainActivity extends AppCompatActivity {

    GregorianCalendar calender;
    CalenderGridAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager()));

        // Give the PagerSlidingTabStrip the ViewPager
        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(viewPager);
        getSupportActionBar().hide();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     * //TODO in eigene Datei und eigene Klasse nicht als static class hier
     */
    public static class PlaceholderFragment extends Fragment implements View.OnClickListener {

        GregorianCalendar gregorianCalendar;
        CalenderGridAdapter calenderGridAdapter;
        TextView monthDisplay;
        private String months[];

        public PlaceholderFragment() {
            gregorianCalendar = new GregorianCalendar();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_calender_view, container, false);
            GridView gridView = (GridView)rootView.findViewById(R.id.gridView);
            calenderGridAdapter = new CalenderGridAdapter(rootView.getContext(), gregorianCalendar);
            calenderGridAdapter.refreshDays();
            gridView.setAdapter(calenderGridAdapter);
            calenderGridAdapter.notifyDataSetChanged();
            monthDisplay = (TextView)rootView.findViewById(R.id.currentMonth);

            months = getResources().getStringArray(R.array.month_names);

            setNewMonth(gregorianCalendar.get(GregorianCalendar.MONTH));
            Toast.makeText(rootView.getContext(), ""+container.getChildCount(), Toast.LENGTH_LONG).show();
            setImageButtonOnClickListener((ViewGroup)rootView);
            return rootView;
        }


        private void setImageButtonOnClickListener(ViewGroup container)
        {
            int count = container.getChildCount();
            for (int i = 0; i < count; i++) {
                View v = container.getChildAt(i);
                if (v instanceof ImageButton) {
                   v.setOnClickListener(this);
                } else if (v instanceof ViewGroup) {
                    //recurse through children
                    setImageButtonOnClickListener((ViewGroup) v);
                }
            }
        }

        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.lastMonth:
                    previousMonthClicked();
                    break;
                case R.id.nextMonth:
                    nextMonthClicked();
            }
        }

        /**
         * onclick method for the previous month button
         */
        public void previousMonthClicked()
        {
            gregorianCalendar.add(GregorianCalendar.MONTH, -1);
            calenderGridAdapter.refreshDays();
            calenderGridAdapter.notifyDataSetChanged();
            setNewMonth(gregorianCalendar.get(GregorianCalendar.MONTH));
        }

        public void nextMonthClicked()
        {
            gregorianCalendar.add(GregorianCalendar.MONTH, 1);
            calenderGridAdapter.refreshDays();
            calenderGridAdapter.notifyDataSetChanged();
            setNewMonth(gregorianCalendar.get(GregorianCalendar.MONTH));
        }

        private void setNewMonth(int month)
        {
            monthDisplay.setText(months[month]);
        }
    }
}
